<?php // $Id$
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
<!--[if IE]><?php print phptemplate_get_ie_styles(); ?><![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>

<div id="header">
  <div class="header limiter">
    <?php if ($logo): ?>
    <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" id="logo" /></a>
    <?php endif; ?>
    
    <div id="sitename">
      <?php if ($site_name): ?>
      <h1><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><?php print check_plain($site_name); ?></a></h1>
      <?php endif; ?>
  
      <?php if ($site_slogan): ?>
      <span id="siteslogan"><?php print check_plain($site_slogan); ?></span>
      <?php endif; ?>
    </div> 
          
    <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>  
  </div>
</div>

<div class="clear">
  <?php if ($uheader): ?><div class="uheader limiter"><?php print $uheader ?></div><?php endif; ?>
</div>
  
<div id="navigation1">  
  <div class="mainnav limiter">  
    <?php if (isset($primary_links)) : ?>
      <div class="nav"><?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?></div>     
    <?php endif; ?>    
    <div class="userbar">
      <?php if ($user_links) print theme('links', $user_links) ?>
     </div>      
  </div>        
</div>
 
<?php if (isset($secondary_links)) : ?> 
<div id="navigation2">       
  <div class="mainnav limiter">      
      <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>    
  </div>  
        
</div>  
<?php endif; ?>
<div class="clear"></div>
    
<div id="container" class="limiter"> <!-- "showgrid" -->

    
  <div id="wrapper">
    <div id="main" class="userpage">
      <?php print $breadcrumb; ?>
       
      
  <div class="clear marg">
        
    <div class="sidebar userside1"> 	  
      <?php if ($tabs): ?>
      <div id="tabs">
        <div class="page-tabs"><ul class="links"><?php print $tabs ?></ul></div>
          <?php if ($tabs2): ?><div class="page-tabs clear"><ul class="links"><?php print $tabs2 ?></ul></div><?php endif; ?>
        </div>
        <?php endif; ?>
      </div>
           
      <div class="content userside2">
        <?php if ($content_top): ?><div class="clear"><?php print $content_top ?></div><?php endif; ?> 
        <h2 class="page-title"><?php if ($title) print $title ?></h2>	      
        <div class="clear-block"></div>
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <?php print $help; ?>
        <div class="clear-block"><?php print $content ?></div>
        <?php if ($content_bottom): ?><div class="clear"><?php print $content_bottom ?></div><?php endif; ?>
    </div> 
                 
  </div>
          
    </div>
        
  </div>
    
  <?php if ($left): ?><div id="sidebar-left" class="sidebar"><?php print $left ?></div><?php endif; ?>      
  <?php if ($right): ?><div id="sidebar-right" class="sidebar"><?php print $right ?></div><?php endif; ?>    
    <div class="push"></div>

</div>
  
  <div id="footer">
    <div class="footer limiter">
      <div class="inside">
        <?php print $footer_message . $footer ?> <?php print $feed_icons ?>
      </div>
    </div>
  </div>

<?php print $closure ?>
</body>
</html>