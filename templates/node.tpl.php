<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
<?php if ($page == 0): ?>
  <h2 class="page_title"><?php if (node_access('update', $node) || $is_admin) : ?><a href="<?php print url('node/'. $node->nid .'/edit') ?>" class="editlink"><img src="/<?php print $base_path.$directory ?>/images/edit.png" alt="<?php print t('Edit') ?>" title="<?php print t('Edit') ?>" /></a><?php endif; ?> <a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>
  <div class="content clear-block">        
    <?php print $content ?>    
    <?php if ($node->readmore and !$page): ?>
      <p><a class="read_more" title="<?php print t('Read more');?>" href="<?php print $node_url?>"><?php print t('Read more');?></a></p>
    <?php endif;?>      
  </div>
    <div class="clear-block">      
    <?php if ($terms): ?>
      <div class="terms clear-block"><?php print $terms ?></div>
    <?php endif; ?>      
    <div class="node_data clear">       
      <?php if ($submitted): ?>      
        <?php if (!empty($submitted)): ?>
          <div class="submitted clear-block">
            <?php if ($picture): print $picture; endif; ?>
            <div class="node_submitted"><?php print $submitted ?></div>
          </div>
        <?php endif; ?>      
      <?php endif; ?>         
      <?php if (!empty($links)): ?><div class="nodelinks clear-block"><?php print $links ?></div><?php endif; ?>      
    </div>
  </div>
</div>