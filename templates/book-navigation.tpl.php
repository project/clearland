<?php
?>
<?php if ($tree || $has_links): ?>
  <div id="book-navigation-<?php print $book_id; ?>" class="book-navigation">
    <?php print $tree; ?>

    <?php if ($has_links): ?>
    <div class="page-links clear-block">
      <?php if ($prev_url) : ?>
        <a href="<?php print $prev_url; ?>" class="page-previous vtip" title="<?php print $prev_title; ?>"><img src="<?php print base_path().path_to_theme()?>/images/spacer.gif" alt="<?php print $prev_title; ?>" title="<?php print $prev_title; ?>" /></a>
      <?php endif; ?>
      <?php if ($parent_url) : ?>
        <a href="<?php print $parent_url; ?>" class="page-up vtip" title="<?php print t('up'); ?>"><img src="<?php print base_path().path_to_theme()?>/images/spacer.gif" alt="<?php print t('up'); ?>" title="<?php print t('up'); ?>" /></a>
      <?php endif; ?>
      <?php if ($next_url) : ?>
        <a href="<?php print $next_url; ?>" class="page-next vtip" title="<?php print $next_title; ?>"><img src="<?php print base_path().path_to_theme()?>/images/spacer.gif" alt="<?php print $next_title; ?>" title="<?php print $next_title; ?>" /></a>
      <?php endif; ?>
    </div>
    <?php endif; ?>

  </div>
<?php endif; ?>