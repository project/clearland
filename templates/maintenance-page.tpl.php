<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
<!--[if lt IE 7]>
  <?php print phptemplate_get_ie_styles(); ?>
<![endif]-->
</head>
<body>
<div id="container" class="limiter">
	<div class="errorBlock"><?php print $content ?></div>
</div>
<?php print $closure ?>
</body>
</html>