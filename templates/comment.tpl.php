<?php 
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">
  <div class="comment-info">     
    <?php if ($picture): print $picture; endif; ?> 
      <div class="data">
        <div class="auth"><?php print $author ?></div> 
        <div class="subm"><?php print $date ?></div>
        <div class="sharp"><?php print $title ?></div> 
        <div class="links"><?php print $links ?></div>
        <?php if (!empty($new)): ?><div class="new"><a id="new" class='new'><?php print('New') ?></a></div><?php endif; ?>
      </div>
   </div>
  <?php if (!empty($content)): ?>
    <div class="content">
      <?php print $content ?>
    </div>
  <?php endif; ?>      
</div>