 Features

  CSS-based (tableless) layout;
  1, 2, or 3 columns;
  4 original color schemes are prepared Green, Blue, Orange, Black);
  Fluid and fixed width (100%, 100% with max-width and 980px);
  Multiple font selection options;
  5 regions;
  Quick block, menu and view editing links
  CSS3 options;
  User picture in profile, comments and post
  Primary links and secondary links
  Supports custom logo and favicon
  Support features like site name, slogan, mission, meta.

Admin settings (color scheme, font-family, layout width, CSS3 options, meta-data)

Full theming:
  Poll
  Node
  Comment
  Book
  Pagination

Valid xHTML 1.0 Strict

Verified and tested with

  IE 8, Mozilla Firefox 3, Safari 4/5, Opera 9/10, Chrome 6/7 (Windows)
  Mozilla Firefox 3, Opera 9/10, Chrome 6 (Linux)
  Safari 4/5 (Mac OS)
